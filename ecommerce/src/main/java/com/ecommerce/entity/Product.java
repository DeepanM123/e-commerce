package com.ecommerce.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
@Entity
public class Product {
	
	@Id
	@GeneratedValue
	private int productid;
	private String productname;
	private double cost;
	private int rating;
	private int qundity;
	public int getQundity() {
		return qundity;
	}
	public void setQundity(int qundity) {
		this.qundity = qundity;
	}
	public int getProductid() {
		return productid;
	}
	public void setProductid(int productid) {
		this.productid = productid;
	}
	public String getProductname() {
		return productname;
	}
	public void setProductname(String productname) {
		this.productname = productname;
	}
	public double getCost() {
		return cost;
	}
	public void setCost(double cost) {
		this.cost = cost;
	}
	public int getRating() {
		return rating;
	}
	public void setRating(int rating) {
		this.rating = rating;
	}
	
	public Product() {
	//create object
	}

	@ManyToOne
	@JoinColumn(name="fk_category")
	Category category;
	
}
