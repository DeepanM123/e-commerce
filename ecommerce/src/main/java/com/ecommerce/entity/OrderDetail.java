package com.ecommerce.entity;




import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


@Entity
public class OrderDetail {
	@Id
	@GeneratedValue
	private int orderid;
	private int productid;
	private int rating;
	private String review;
	public int getOrderid() {
		return orderid;
	}
	public void setOrderid(int orderid) {
		this.orderid = orderid;
	}
	public int getProductid() {
		return productid;
	}
	public void setProductid(int productid) {
		this.productid = productid;
	}
	public int getRating() {
		return rating;
	}
	public void setRating(int rating) {
		this.rating = rating;
	}
	public String getReview() {
		return review;
	}
	public void setReview(String review) {
		this.review = review;
	}

	@ManyToOne
	@JoinColumn(name="fk_user")
	UserDetail userDetail;
	public UserDetail getUserDetail() {
		return userDetail;
	}
	public void setUserDetail(UserDetail userDetail) {
		this.userDetail = userDetail;
	}
	public OrderDetail(UserDetail userDetail) {
		super();
		this.userDetail = userDetail;
	}
	public OrderDetail() {
//create object
	}
	
}
