package com.ecommerce.service;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.ecommerce.dto.ProductResponseDto;
@Service
public interface ProductBuyService {

	public ResponseEntity<ProductResponseDto> productbuybyid(int productid);
}
