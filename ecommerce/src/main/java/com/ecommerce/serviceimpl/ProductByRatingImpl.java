package com.ecommerce.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ecommerce.entity.Product;
import com.ecommerce.exceptions.ProductsNotFoundException;
import com.ecommerce.repository.CategoryRepository;
import com.ecommerce.repository.ProductRepository;
import com.ecommerce.service.ProductsByRating;

@Service
public class ProductByRatingImpl implements ProductsByRating {

	@Autowired
	ProductRepository productRepository;
	@Autowired
	CategoryRepository categoryRepository;

	public List<Product> getallproducts(String catelogname) throws ProductsNotFoundException {
		List<Product> products = productRepository.findAllByCategory_categorynameOrderByRatingDesc(catelogname);
		if (products.isEmpty()) {
			throw new ProductsNotFoundException("no products are avaiable");
		}
		return products;
	}
}
